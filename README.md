# rpi stuff
Some helpfull things/fixes for the Raspberry Pi.

## temps.sh
A simple bash script to measure the rpi's GPU and CPU temperature, since (at least for me) `lm-sensor` would not work.

### Prerequisites
`bc` needs to be installed, i.e. `sudo apt install bc`.

### Setup
Make the script executable via `sudo chmod +x temps.sh`.
Call the script via `./temps.sh`.
If you want to better monitor the temperature you might want to run the script in the following way:
`watch -n 1 ./temps.sh`
This will refresh the output every second.

## Screen Tearing
If screen tearing occurs it is most likely because of the compositor. The fix here disables the compositor and prevents it from starting after a reboot.

The following command prevents the compositor from running at startup (and also makes a backup, if one wants to enable it at some point)
`sudo mv /etc/xdg/autostart/xcompmgr.desktop /etc/xdg/autostart/xcompmgr.desktop.donotrun`
The next command disables the compositor
`killall xcompmgr `
You might need to restart your pi for this to take effect.

If you want to reenable the compositor just run 
`sudo mv /etc/xdg/autostart/xcompmgr.desktop.donotrun /etc/xdg/autostart/xcompmgr.desktop`
and reboot the system.