#!/bin/bash

#################################################
#						#
#	by Alucard17, alucard17@gitlab		#
#						#
#################################################

gpu=$(/opt/vc/bin/vcgencmd measure_temp)
cpu=$(cat /sys/class/thermal/thermal_zone0/temp)
num=1000
cpu1=$(echo "scale=1; $cpu / $num;" | bc)
cpu2=temp=$cpu1"'"C

echo Temps:
echo GPU: $gpu
echo CPU: $cpu2